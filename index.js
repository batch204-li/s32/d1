let http = require("http");
// Use the "require" directive to load Node.js modules
// HTTP is a protocol that allows the fetching of resources such as HTML documents

http.createServer(function (request, response) {

	if (request.url == "/items" && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data retreived from the database');
	}


}).listen(4000);
// A port is a virtual point where network connections start and end
// The server will be assigned to port 4000 via the "listen(4000"

console.log("Server running at locahost:4000");
// When server is running, console will print the message